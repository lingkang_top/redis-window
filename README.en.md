# redis-window

#### Description
The current branch version is the latest redis version master branch<br>
To download different versions of redis-window, please switch to different branches to download. <br>
You can also go directly to the release download: https://gitee.com/lingkang_top/redis-window/releases

6.  https://gitee.com/gitee-stars/)

#### Introduction

Compile the redis source code into a window to run an exe, which is only used for the development and learning of the high version of redis **feature**, **performance** cannot be compared with running on Linux. :ghost:

Redis official website has stated that there is no windows version: https://redis.io/topics/introduction

Only package redis into window executable exe to facilitate the use of the development environment.

## Latest compilation schedule

```shell
2022-11-28 redis-7.0
2021-05-19 redis-6.0.13
2021-05-19 redis-6.0.12
2021-01-25 redis-6.0.10
```

## Specify the configuration file to run

```cmd
redis-server.exe ./redis.conf
```

![输入图片说明](https://images.gitee.com/uploads/images/2020/1222/220122_96ef16ad_2159235.png "pic.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1222/220201_992b56a1_2159235.png "pic.png")