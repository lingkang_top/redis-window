# redis-window
> 当前分支版本为最新redis版本master分支<br>
> 需要下载不同版本redis-window，请自行切换不同分支下载即可。<br>
> 也可以直接前往release下载：https://gitee.com/lingkang_top/redis-window/releases
#### 介绍
将redis源码编译为window运行exe，仅用于高版本redis **特性** 的开发、学习， **性能** 无法与Linux上运行相比较。 :ghost: 

redis官网已声明无windows版本：https://redis.io/topics/introduction

仅将redis打包成window可执行exe，以方便开发环境使用。

```shell
2022-11-28 redis-7.0.5
2021-05-19 redis-6.0.13
2021-05-19 redis-6.0.12
2021-01-25 redis-6.0.10
```

## 指定配置文件运行

```cmd
redis-server.exe ./redis.conf
```

![输入图片说明](https://images.gitee.com/uploads/images/2020/1222/220122_96ef16ad_2159235.png "pic.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1222/220201_992b56a1_2159235.png "pic.png")